import unittest

try:
    from ..utils import pin2name, name2pin
except ImportError:
    from sysfsgpio.utils import pin2name, name2pin


class TestFunctions(unittest.TestCase):
    PIN_NAMES = []

    @classmethod
    def setUpClass(cls):
        for port in 'ABCDEFGHIJKL':
            for pin in range(32):
                name = 'P{}{}'.format(port, pin)
                cls.PIN_NAMES.append(name)

    def test_pin2name_not_int(self):
        for arg in [tuple(),
                    dict(),
                    3.14,
                    '']:
            with self.subTest(type=type(arg)):
                with self.assertRaises(TypeError):
                    pin2name(arg)

    def test_pin2name_negative(self):
        with self.assertRaises(ValueError):
            pin2name(-1)

    def test_pin2name_out_of_range(self):
        with self.assertRaises(ValueError):
            pin2name(384)

    def test_pin2name(self):
        for idx, name in enumerate(self.PIN_NAMES):
            rname = pin2name(idx)
            self.assertEqual(rname, name)

    def test_name2pin_not_str(self):
        for arg in [tuple(),
                    dict(),
                    3.14,
                    13]:
            with self.subTest(type=type(arg)):
                with self.assertRaises(TypeError):
                    name2pin(arg)

    def test_name2pin_invalid_format(self):
        for arg in ['A12', 'Py13', 'PA3.14']:
            with self.subTest(arg=arg):
                with self.assertRaises(ValueError):
                    name2pin(arg)

    def test_name2pin(self):
        for idx, name in enumerate(self.PIN_NAMES):
            ridx = name2pin(name)
            self.assertEqual(ridx, idx)


if __name__ == '__main__':
    unittest.main()
