import setuptools

with open('./sysfsgpio/version.py') as fd:
    exec(fd.read())

with open("README.md", "r") as fd:
    long_description = fd.read()

setuptools.setup(
    name="sysfsgpio",
    version=VERSION,
    author="Kacper Kubkowski",
    author_email="kkubkowski@gmail.com",
    description="Sysfs GPIO utility",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/kkubkowski/sysfsgpiolib",
    packages=['sysfsgpio'],
    package_data={
        'sysfsgpio': [
            'resources/gpio-exporter.service',
            'resources/gpio.rules'
        ]
    },
    entry_points={
        'console_scripts': [
            'sysfsgpio-install = sysfsgpio.utils:install',
            'sysfsgpio-install-service = sysfsgpio.utils:install_service',
            'sysfsgpio-install-rules = sysfsgpio.utils:install_rules'
        ]
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
    ],
    test_suite='nose.collector',
    tests_require=['nose'],
)
