
VERSION_FILE = sysfsgpio/version.py
REPO_VERSION = $(shell git describe --tags)
DIST_VERSION = $(shell git describe --tags --abbrev=0)
VCS_TAIL = $(strip $(subst $(DIST_VERSION),,$(REPO_VERSION)))
ifneq ($(VCS_TAIL),)
VCS_TAIL := $(subst -,.,$(patsubst -%,+%,$(VCS_TAIL)))
endif
VERSION = $(patsubst v%,%,$(DIST_VERSION))$(VCS_TAIL)
TESTS_DIR = sysfsgpio/tests
PACKAGE_NAME = trimarlib-sysfsgpio

ifeq ($(OS),Windows_NT)
PYTHON ?= python
else
PYTHON ?= python3
endif


.PHONY: all clean package version test

all: test package

package: version
	@echo "Creating source distribution..."
	$(PYTHON) setup.py sdist bdist_wheel
	@echo " "

clean:
	rm -rf dist *.egg-info build

version:
	@echo "Generating version file..."
	@echo "VERSION = '$(VERSION)'" > $(VERSION_FILE)
	@echo "Version is $(VERSION)"
	@echo " "

test: version
	@echo "Running unit tests..."
	$(PYTHON) setup.py test
	@echo " "
